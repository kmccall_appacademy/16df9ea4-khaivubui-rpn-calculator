class RPNCalculator
  def value
    @num_array[-1]
  end

  def initialize
    @num_array = []
  end

  def push(num)
    @num_array.push(num)
  end

  def plus
    @num_array[-2] = @num_array[-2] + @num_array[-1]
    @num_array.pop
  rescue
    raise('calculator is empty')
  end

  def minus
    @num_array[-2] = @num_array[-2] - @num_array[-1]
    @num_array.pop
  rescue
    raise('calculator is empty')
  end

  def times
    @num_array[-2] = @num_array[-2] * @num_array[-1]
    @num_array.pop
  rescue
    raise('calculator is empty')
  end

  def divide
    @num_array[-2] = @num_array[-2] / @num_array[-1].to_f
    @num_array.pop
  rescue
    raise('calculator is empty')
  end

  def tokens(str)
    str.split(' ').map do |el|
      if '+-*/'.include?el
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def evaluate(str)
    tokens(str).each do |el|
      if el.class == Fixnum
        @num_array.push(el)
      else
        plus if el == :+
        minus if el == :-
        times if el == :*
        divide if el == :/
      end
    end
    value
  end
end
